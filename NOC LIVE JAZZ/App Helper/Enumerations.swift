//
//  Enumerations.swift
//  NOC LIVE JAZZ
//
//  Created by Mac on 10/14/17.
//  Copyright © 2017 macbook. All rights reserved.
//

import Foundation
extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    
    func Count() -> Int
    {
        return self.characters.count
    }
    func isStringEmpty() -> Bool
    {
        return self.isStringEmpty()
    }
    func isValidEmail() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
}

