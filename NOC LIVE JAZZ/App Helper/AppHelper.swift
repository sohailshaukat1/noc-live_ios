//
//  AppHelper.swift
//  NOC LIVE JAZZ
//
//  Created by Mac on 10/14/17.
//  Copyright © 2017 macbook. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import Alamofire

let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
var activityIndicator:UIActivityIndicatorView?

//This class contains common Methods used in app.

class AppHelper: NSObject, UINavigationControllerDelegate, UIAlertViewDelegate {
    
    //MARK: - Getting Storyboard
    class func Storyboard() -> UIStoryboard {
        
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    //  MARK:- NSUser Defaults
    
    //MARK: User default
    
    class func saveIntValue(value:Int , key:String)
    {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key as String)
        defaults.synchronize()
    }
    // save to user default
    class func saveToUserDefault (value: AnyObject? , key: String!) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key! as String)
        defaults.synchronize()
    }
    
    // Get from user default
    
    class func userDefaultForKey ( key: String?) -> String{
        let defaults = UserDefaults.standard
        let value = defaults.string(forKey: key! as String)
        if (value != nil) {
            return value!
        }
        return ""
    }
    
    // Remove from user default
    class  func removeFromUserDefaultForKey(key: NSString!) {
        let defaults = UserDefaults.standard
        let value = defaults.string(forKey: key! as String)
        if (value != nil) {
            defaults.removeObject(forKey: key! as String)
        }
        defaults.synchronize()
        
    }
    // TimeAgo
    class func timeAgoSince( d: Date) -> String {
        
        let calendar = Calendar.current
        let now = Date()
        let date = calendar.date(byAdding: .minute, value: -6, to: now)
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        var components = (calendar as NSCalendar).components(unitFlags, from: d, to: date!, options: [])
        
        
        if let year = components.year, year >= 2 {
            return "\(year) years"
        }
        
        if let year = components.year, year >= 1 {
            return "Last year"
        }
        
        if let month = components.month, month >= 2 {
            return "\(month) months "
        }
        
        if let month = components.month, month >= 1 {
            return "Last month"
        }
        
        if let week = components.weekOfYear, week >= 2 {
            return "\(week)w "
        }
        
        if let week = components.weekOfYear, week >= 1 {
            return "Last week"
        }
        
        if let day = components.day, day >= 2 {
            return "\(day)d "
        }
        
        if let day = components.day, day >= 1 {
            return "Yesterday"
        }
        
        if let hour = components.hour, hour >= 2 {
            return "\(hour)h"
        }
        
        if let hour = components.hour, hour >= 1 {
            return "\(hour)h"
        }
        
        if let minute = components.minute, minute >= 2 {
            return "\(minute)m"
        }
        
        if let minute = components.minute, minute >= 1 {
            return "\(minute)m"
        }
        
        
        if let second = components.second, second >= 0 {
            return "Just now"
        }
        
        
        return "Just now"
        
    }
    
    
    class func dictUserDefaultsForKey(key: String?)->AnyObject? {
        let defaults = UserDefaults.standard
        if defaults.value(forKey: key!) != nil{
            let data = defaults.value(forKey: key!) as AnyObject
            return data
        }
        return nil
    }
    
    //MARK: ************* CHECK NETWORK REACHABILITY *************
    class func networkReachable() -> Bool {
        return (NetworkReachabilityManager()?.isReachable)!
    }
    
    
    
    //MARK: AlertView
    class func showALertWithTag(title:String, message:String?,delegate:AnyObject!, cancelButtonTitle:String?, otherButtonTitle:String?)
    {
        
        
        var alertController = UIAlertController()
        
        alertController = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.alert)
        
        
        
        let ok:UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(ok)
        let topViewController = APPDELEGATE.window?.rootViewController
        topViewController?.present(alertController, animated: true, completion: nil)
    }
}

// MARK:- Global Methods

//Toast Message
func showHudWithMessage(message:String, inViewConroller vc:UIViewController, animated:Bool, hideAfter seconds:TimeInterval) {
    
    let hud = MBProgressHUD.showAdded(to: vc.view, animated: true)
    //hud.offset.y = 200.0
    hud.bezelView.color = UIColor(red: 255/255.0, green: 138/255.0, blue: 51/255.0, alpha: 1)
    hud.contentColor = UIColor.white
    hud.mode = MBProgressHUDMode.text
    hud.detailsLabel.text = message
    hud.show(animated: true)
    hud.hide(animated: true, afterDelay: seconds)
}

//MARK:- Activity Indicator

func showActivityIndicator(decision:Bool, inViewConroller vc:UIViewController, animated:Bool) {
    if(decision) {
        MBProgressHUD.showAdded(to: vc.view, animated: animated)
    }
    else {
        MBProgressHUD.hide(for: vc.view, animated: animated)
    }
}
