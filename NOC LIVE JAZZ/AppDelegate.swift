//
//  AppDelegate.swift
//  NOC LIVE JAZZ
//
//  Created by macbook on 10/10/2017.
//  Copyright © 2017 macbook. All rights reserved.
//

import UIKit
import UserNotifications
import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static var base_url = "https://jazznoc.jazz.com.pk/JazzNocApp/"
    let mainStoryBoard : UIStoryboard = UIStoryboard(name:"Main", bundle : nil)
    let defaults = UserDefaults.standard
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Enable IQKeybaord
        IQKeyboardManager.sharedManager().enable = true
        
        if #available(iOS 9.0, *) {
            let notificationSettings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
            
            UIApplication.shared.registerForRemoteNotifications()
            
        } else {
            
            let center : UNUserNotificationCenter = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in
                if( !(error != nil) )
                {
                    UIApplication.shared.registerForRemoteNotifications()
                    print( "Push registration success.")
                }
            }
        }
        
        let isuserExist = AppHelper.userDefaultForKey(key: "isUserRegistered")
         if isuserExist == "true"
         {
           //Move to login view controller
            let controller  = self.mainStoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
            
            var Nav = storyBoard.instantiateViewController(withIdentifier: "loginNav")
            Nav = UINavigationController(rootViewController: controller)
            window?.rootViewController = Nav
            window?.makeKeyAndVisible()
         }
        
        else
         {
           // show number screen
        }
        
        return true
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print("Error = ",error.localizedDescription)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        let APS = userInfo["aps"] as! NSDictionary
        print(APS)
        let controller = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let alertText = APS.object(forKey: "alert") as! String
        DispatchQueue.main.async {
            
            var Nav = self.storyBoard.instantiateViewController(withIdentifier: "nav")
            Nav = UINavigationController(rootViewController: controller)
            
            self.window?.rootViewController = Nav
            self.window?.makeKeyAndVisible()
        }
        
        
        
        if (UIApplication.shared.applicationState == UIApplicationState.background) {
            completionHandler(UIBackgroundFetchResult.noData);
            return;
        }
        completionHandler(.newData)
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        print("Registration succeeded! Token: ", token)
        
    }


}

