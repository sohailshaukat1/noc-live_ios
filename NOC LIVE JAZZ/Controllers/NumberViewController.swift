//
//  NumberViewController.swift
//  NOC LIVE JAZZ
//
//  Created by Mac on 10/14/17.
//  Copyright © 2017 macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD
class NumberViewController: UIViewController,UITextFieldDelegate {

    //Mark: Outlets
    @IBOutlet weak var TxtNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set placeholder Color in TextField
        TxtNumber .setValue(UIColor(red:255, green:255, blue:255, alpha:1.0), forKeyPath: "_placeholderLabel.textColor")
        self.TxtNumber.delegate = self
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //Mark: Actions
    @IBAction func nextAction(_ sender: Any) {
        
        if (checkForValidation()){
            
            if(AppHelper.networkReachable()){
                
                let Txt_number = self.TxtNumber.text!
                let validatenewCellNo = "api/otp/validatenewCellNo/" + Txt_number
                // Show Activity Indicator
                showActivityIndicator(decision: true, inViewConroller: self, animated: true)
                let url = AppDelegate.base_url + validatenewCellNo
                
                //Background thread
                DispatchQueue.global().async
                    {
                        
                        Alamofire.request(url, method: .get,parameters: nil).responseJSON(completionHandler: { (response) in
                            
                            print("Response\(response.result)")
                            
                            switch response.result
                            {
                                
                            case .success(let dataFromAF):
                                
                                //Get JSON object from Alamofire into result
                                let result = JSON(dataFromAF)
                                print(result)
                                
                                if result["message"].string == "Cell No validated"
                                {
                                    if let jsonDict = response.result.value as? NSDictionary{
                                        
                                        print("My jsonVlaue is : \(jsonDict)")
                                        AppHelper.saveToUserDefault(value: jsonDict.object(forKey: "api") as AnyObject, key: "OTP_APi")
                                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                                        controller.number = (self.TxtNumber.text?.trim())!
                                        self.navigationController?.pushViewController(controller, animated: true)
                                    }
                                    
                                }
                                
                                else
                                {
                                    showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                                   // print(result["error"].string!)
                                    let refreshAlert = UIAlertController(title: "Error", message: result["error"].string!, preferredStyle: UIAlertControllerStyle.alert)
                                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                        print("Handle Ok logic here")
                                    }))
                                    refreshAlert.view.tintColor = UIColor.red
                                    
                                    self.present(refreshAlert, animated: true, completion: nil)
                                    //showHudWithMessage(message: result["error"].string!, inViewConroller: self, animated: true, hideAfter: 3)
                                }
                                
                                break
                            case .failure(let error):
                                DispatchQueue.main.async {
                                    showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                                   // print(error.localizedDescription)
                                    let refreshAlert = UIAlertController(title: "Error", message: "Your cell number is not registered please contact OSS team", preferredStyle: UIAlertControllerStyle.alert)
                                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                        print("Handle Ok logic here")
                                    }))
                                    refreshAlert.view.tintColor = UIColor.red
                                    
                                    self.present(refreshAlert, animated: true, completion: nil)
                                   // showHudWithMessage(message: "Your cell number is not registered please contact OSS team.", inViewConroller: self, animated: true, hideAfter: 3)
                                    
                                }
                                break
                            }
                        })
                }
            }
                
            else
            {
                let refreshAlert = UIAlertController(title: "Error", message: "Internet not Available", preferredStyle: UIAlertControllerStyle.alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    print("Handle Ok logic here")
                }))
                refreshAlert.view.tintColor = UIColor.red
                
                present(refreshAlert, animated: true, completion: nil)
                //showHudWithMessage(message: "Internet not Available!", inViewConroller: self, animated: true, hideAfter: 2)
            }
        }
        
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if  (textField == self.TxtNumber) && !(string == "")
        {
            
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 12 // Bool
        }
        return true
        
        
    }
    
    //Mark: Functions
    func checkForValidation() -> Bool {
        
        // Get text from textfield
        let number        = self.TxtNumber.text?.trim()
        
        if  number == ""
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Please enter valid cell no", preferredStyle: UIAlertControllerStyle.alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
            }))
            refreshAlert.view.tintColor = UIColor.red
            
            present(refreshAlert, animated: true, completion: nil)
            //showHudWithMessage(message: "Please enter valid cell no", inViewConroller: self, animated: true, hideAfter: 2)
            return false
        }
        
        return true
    }
}
