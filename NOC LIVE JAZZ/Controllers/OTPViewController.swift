//
//  OTPViewController.swift
//  NOC LIVE JAZZ
//
//  Created by Mac on 10/14/17.
//  Copyright © 2017 macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class OTPViewController: UIViewController,UITextFieldDelegate {

    //Mark: Outlets
    @IBOutlet weak var TxtOTP: UITextField!
    //MArk: Variables
    var number : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set placeholder Color in TextField
        TxtOTP .setValue(UIColor(red:255, green:255, blue:255, alpha:1.0), forKeyPath: "_placeholderLabel.textColor")
      self.TxtOTP.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   // Actions
    @IBAction func NextAction(_ sender: Any) {
        
        if (checkForValidation()){
            
            if(AppHelper.networkReachable()){
                let Api_token = AppHelper.userDefaultForKey(key: "OTP_APi")
                
                let otpCode = number + "/\(String(describing: self.TxtOTP.text!.trim()))/\(Api_token)"
                let validatenewCellNo = "api/otp/registernewuser/" + otpCode
                // Show Activity Indicator
                showActivityIndicator(decision: true, inViewConroller: self, animated: true)
                let url = AppDelegate.base_url + validatenewCellNo
                
                //Background thread
                DispatchQueue.global().async
                    {
                        
                        Alamofire.request(url, method: .get,parameters: nil).responseJSON(completionHandler: { (response) in
                            
                            print("Response\(response.result)")
                            
                            switch response.result
                            {
                                
                            case .success(let dataFromAF):
                                
                                //Get JSON object from Alamofire into result
                                let result = JSON(dataFromAF)
                                print(result)
                                
                                if result["message"] == "User registered successfully!"
                                {
                                    if let jsonDict = response.result.value as? NSDictionary{
                                        
                                        print("My jsonVlaue is : \(jsonDict)")
                                        AppHelper.saveToUserDefault(value: "true" as AnyObject, key: "isUserRegistered")
                                        showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                                       // showHudWithMessage(message: result["message"].string!, inViewConroller: self, animated: true, hideAfter: 4)
                                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                        self.navigationController?.pushViewController(controller, animated: true)
                                    }
                                }
                                    
                                else
                                {
                                    showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                                    let refreshAlert = UIAlertController(title: "Error", message: result["message"].string!, preferredStyle: UIAlertControllerStyle.alert)
                                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                        print("Handle Ok logic here")
                                    }))
                                    refreshAlert.view.tintColor = UIColor.red
                                    
                                    self.present(refreshAlert, animated: true, completion: nil)
                                   // showHudWithMessage(message: result["message"].string!, inViewConroller: self, animated: true, hideAfter: 3)
                                }
                                
                                break
                            case .failure(let error):
                                DispatchQueue.main.async {
                                    showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                                    let refreshAlert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                        print("Handle Ok logic here")
                                    }))
                                    refreshAlert.view.tintColor = UIColor.red
                                    
                                    self.present(refreshAlert, animated: true, completion: nil)
                                  //  showHudWithMessage(message: error.localizedDescription, inViewConroller: self, animated: true, hideAfter: 3)
                                    
                                }
                                break
                            }
                        })
                }
            }
                
            else
            {
                let refreshAlert = UIAlertController(title: "Error", message: "Internet not Available", preferredStyle: UIAlertControllerStyle.alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    print("Handle Ok logic here")
                }))
                refreshAlert.view.tintColor = UIColor.red
                
                present(refreshAlert, animated: true, completion: nil)
               // showHudWithMessage(message: "Internet not Available!", inViewConroller: self, animated: true, hideAfter: 2)
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if  (textField == self.TxtOTP) && !(string == "")
        {
            
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 5 // Bool
        }
        return true
        
        
    }
    
    
    //Mark: Functions
    func checkForValidation() -> Bool {
        
        // Get text from textfield
        let opt_number        = self.TxtOTP.text?.trim()
        
        if  opt_number == ""
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Enter OTP Code!", preferredStyle: UIAlertControllerStyle.alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
            }))
            refreshAlert.view.tintColor = UIColor.red
            
            present(refreshAlert, animated: true, completion: nil)
            return false
        }
        
        return true
    }

}
