//
//  FluctuationViewController.swift
//  NOC LIVE JAZZ
//
//  Created by Mac on 10/17/17.
//  Copyright © 2017 macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class FluctuationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    
    var fluctuated_array:Array = Array<Fluctuated>()
    var itemsArr:Array = [Fluctuated]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.delegate  = self
        self.tblView.dataSource = self
        //self.tblView.backgroundView?.backgroundColor = UIColor.clear
        self.tblView.reloadData()
       getFluctuatedDetail()

        // Do any additional setup after loading the view.
    }

    func getFluctuatedDetail()
    {
        if(AppHelper.networkReachable()){
            
            // Show Activity Indicator
            showActivityIndicator(decision: true, inViewConroller: self, animated: true)
            let url = AppDelegate.base_url + "api/livenw/fluc/NORTH/2017-10-17/00/3G"
        /* let params = [
                "region"       : "NORTH",
                "date"         : "2017-10-17",
                "time"         : "00",
                "alarm"        : "3G"
                
                ] as [String : Any]*/
            
            //Background thread
            DispatchQueue.global().async
                {
                    
                    Alamofire.request(url, method: .get,parameters: nil).responseJSON(completionHandler: { (response) in
                        
                        print("Response\(response.result)")
                        
                        switch response.result
                        {
                            
                        case .success(let dataFromAF):
                            
                            //Get JSON object from Alamofire into result
                            let result = JSON(dataFromAF)
                            print(result)
                            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                            /*print(result.array!)
                            let ar_ray = result.array!
                            for i in 0...ar_ray.count - 1  {
                                let obj = ar_ray[i].dictionaryObject
                                print(obj!)
                                let fluctuated_data : Fluctuated = Fluctuated()
                                fluctuated_data.main_site = (obj!["mbuNo"]! as! String)
                                fluctuated_data.sitecode = (obj!["siteCode"]! as! String)
                                fluctuated_data.Partial = (obj!["partial"]! as! Int)
                                fluctuated_data.complete = (obj!["complete"]! as! Int)
                                fluctuated_data.total = (obj!["total"]! as! Int)
                               
                                self.fluctuated_array.append(fluctuated_data)
                            }
                            
                            self.tblView.reloadData()*/
                            
                            
                            break
                        case .failure(let error):
                            DispatchQueue.main.async {
                                showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                                let refreshAlert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                    print("Handle Ok logic here")
                                }))
                                refreshAlert.view.tintColor = UIColor.red
                                
                                self.present(refreshAlert, animated: true, completion: nil)
                                //showHudWithMessage(message: error.localizedDescription, inViewConroller: self, animated: true, hideAfter: 3)
                                
                            }
                            break
                        }
                    })
            }
        }
            
        else
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Internet not Available", preferredStyle: UIAlertControllerStyle.alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
            }))
            refreshAlert.view.tintColor = UIColor.red
            
            present(refreshAlert, animated: true, completion: nil)
            //showHudWithMessage(message: "Internet not Available!", inViewConroller: self, animated: true, hideAfter: 2)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fluctuated_array.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // get a reference to our storyboard cell
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "FluctuatedTableViewCell", for: indexPath as IndexPath) as! FluctuatedTableViewCell
        
        cell.lbl_mainSitecode.text = self.fluctuated_array[indexPath.row].main_site
        cell.lbl_sitecode.text = self.fluctuated_array[indexPath.row].sitecode
        cell.lbl_partial.text = String(self.fluctuated_array[indexPath.row].Partial)
        cell.lbl_complete.text = String(self.fluctuated_array[indexPath.row].complete)
        cell.lbl_total.text = String(self.fluctuated_array[indexPath.row].total)
        
        return cell
    }
}
