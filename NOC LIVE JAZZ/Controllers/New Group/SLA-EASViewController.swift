//
//  SLA-EASViewController.swift
//  NOC LIVE JAZZ
//
//  Created by Mac on 10/20/17.
//  Copyright © 2017 macbook. All rights reserved.
//

import UIKit

class SLA_EASViewController: UIViewController {

    
    @IBOutlet weak var tabbar: UIView!
    var tabs = [
        
        ViewPagerTab(title: "Report", image: nil),
        ViewPagerTab(title: "Trend", image: nil)]
    
    var viewPager:ViewPagerController!
    override func viewDidLoad() {
        super.viewDidLoad()
       setUpPagerView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpPagerView()
    {
        let options = ViewPagerOptions(viewPagerWithFrame: self.tabbar.bounds)
        options.tabType = ViewPagerTabType.basic
        options.tabViewTextFont = UIFont.systemFont(ofSize: 16)
        options.tabViewPaddingLeft = 0
        options.tabViewPaddingRight = 0
        options.isTabHighlightAvailable = true
        
        viewPager = ViewPagerController()
        viewPager.options = options
        viewPager.dataSource = self
        viewPager.delegate = self
        
        self.addChildViewController(viewPager)
        self.tabbar.addSubview(viewPager.view)
        viewPager.didMove(toParentViewController: self)
    }

}
extension SLA_EASViewController: ViewPagerControllerDataSource {
    
    func numberOfPages() -> Int {
        return tabs.count
    }
    
    func viewControllerAtPosition(position:Int) -> UIViewController {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisteredUserViewController") as! RegisteredUserViewController
        
       /* vc.itemIndex = "\(tabs[position].title!)"
        if vc.itemIndex == "Images"
        {
            vc.Media_PostArray = Pic_PostArray
        }
        else if vc.itemIndex == "Videos"
        {
            vc.Media_PostArray = Video_PostArray
        }
        else
        {
            vc.Text_PostArray = Text_PostArray
        }*/
        return vc
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
}

extension SLA_EASViewController: ViewPagerControllerDelegate {
    
    func willMoveToControllerAtIndex(index:Int) {
        print("Moving to page \(index)")
    }
    
    func didMoveToControllerAtIndex(index: Int) {
        print("Moved to page \(index)")
    }
}
