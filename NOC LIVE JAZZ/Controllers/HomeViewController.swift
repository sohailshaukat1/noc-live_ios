//
//  HomeViewController.swift
//  NOC LIVE JAZZ
//
//  Created by macbook on 14/10/2017.
//  Copyright © 2017 macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class HomeViewController: UIViewController {

    //Mark: Variables
    var user_rights : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callUserRightApi()
    }

    func callUserRightApi()
    {
        if(AppHelper.networkReachable()){
            
            // Show Activity Indicator
            showActivityIndicator(decision: true, inViewConroller: self, animated: true)
            let uname = AppHelper.userDefaultForKey(key: "username")
            let accessToken = AppHelper.userDefaultForKey(key: "AccessToken")
            let rights_url = "api/users/rights/" + uname
            let headers: HTTPHeaders = [
                "Authorization": "bearer" + " \(accessToken)"
            ]
            
            let url = AppDelegate.base_url + rights_url
            
            //Background thread
            DispatchQueue.global().async
                {
                    
                     Alamofire.request(url, method: .get,parameters: nil,headers:headers).responseJSON(completionHandler: { (response) in
                        
                        print("Response\(response.result)")
                        switch response.result
                        {
                            
                        case .success(let dataFromAF):
                            
                            //Get JSON object from Alamofire into result
                            let result = JSON(dataFromAF)
                            print(result)
                            print(result.array!)
                            let ar_ray = result.array!
                            for i in 0...ar_ray.count - 1  {
                                let obj = ar_ray[i].dictionaryObject
                                print(obj!)
                                print(obj!["groupName"]!)
                                print(obj!["username"]!)
                            }
                            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                        //    let times = result as! [String]
                            //self.user_rights = result.array as! NSMutableArray
                                //    print(self.user_rights.count)
                         //   print(self.user_rights)
                                    //self.updateHome()
                            
                                
                            
                            
                            break
                        case .failure(let error):
                            DispatchQueue.main.async {
                                showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                                showHudWithMessage(message: error.localizedDescription, inViewConroller: self, animated: true, hideAfter: 3)
                                
                            }
                            break
                        }
                    })
            }
        }
            
        else
        {
            showHudWithMessage(message: "Internet not Available!", inViewConroller: self, animated: true, hideAfter: 2)
        }
    }
    func updateHome()
    {
        
    }
}
