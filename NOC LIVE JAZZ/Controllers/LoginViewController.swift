//
//  LoginViewController.swift
//  NOC LIVE JAZZ
//
//  Created by macbook on 11/10/2017.
//  Copyright © 2017 macbook. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class LoginViewController: UIViewController,UITextFieldDelegate {

    //MarK: Outlets
    @IBOutlet weak var TxtuserId: UITextField!
    @IBOutlet weak var TxtPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set placeholder Color in TextField
        TxtuserId .setValue(UIColor.white, forKeyPath: "_placeholderLabel.textColor")
        TxtPassword .setValue(UIColor(red:255, green:255, blue:255, alpha:1.0), forKeyPath: "_placeholderLabel.textColor")
        self.TxtuserId.delegate = self
        self.TxtPassword.delegate = self
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func LoginAction(_ sender: Any) {
        
        if (checkForValidation()){
            
            if(AppHelper.networkReachable()){
                
                // Show Activity Indicator
                showActivityIndicator(decision: true, inViewConroller: self, animated: true)
                let url = AppDelegate.base_url + "token"
                let params = [
                    "grant_type"       : "password",
                    "username"         : self.TxtuserId.text!.trim(),
                    "password"         : self.TxtPassword.text!.trim()
                    
                    ] as [String : Any]
                
                let headers: HTTPHeaders = [
                    "Content-Type": "application/x-www-form-urlencoded"
                ]
                
                //Background thread
                DispatchQueue.global().async
                    {
                        
                       Alamofire.request(url, method: .post,parameters: params,headers:headers).responseJSON(completionHandler: { (response) in
                            
                            print("Response\(response.result)")
                            
                            switch response.result
                            {
                                
                            case .success(let dataFromAF):
                                
                                //Get JSON object from Alamofire into result
                                let result = JSON(dataFromAF)
                                print(result)
                                
                                if result["error"].string == "invalid_grant"
                                {
                                    showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                                    print(result["error_description"].string!)
                                    let refreshAlert = UIAlertController(title: "Error", message: result["error_description"].string!, preferredStyle: UIAlertControllerStyle.alert)
                                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                        print("Handle Ok logic here")
                                    }))
                                    refreshAlert.view.tintColor = UIColor.red
                                    
                                    self.present(refreshAlert, animated: true, completion: nil)
                                   // showHudWithMessage(message: result["error_description"].string!, inViewConroller: self, animated: true, hideAfter: 3)
                                }
                                    
                                else
                                {
                                    
                                    if let jsonDict = response.result.value as? NSDictionary{
                                        
                                        print("My jsonVlaue is : \(jsonDict)")
                                        AppHelper.saveToUserDefault(value: jsonDict.object(forKey: "access_token") as AnyObject, key: "AccessToken")
                                        AppHelper.saveToUserDefault(value: self.TxtuserId.text! as AnyObject, key: "username")
                                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                        self.navigationController?.pushViewController(controller, animated: true)
                                    }
                                }
                                
                                break
                            case .failure(let error):
                                DispatchQueue.main.async {
                                    showActivityIndicator(decision: false, inViewConroller: self, animated: true)
                                    let refreshAlert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                                        print("Handle Ok logic here")
                                    }))
                                    refreshAlert.view.tintColor = UIColor.red
                                    
                                    self.present(refreshAlert, animated: true, completion: nil)
                                    //showHudWithMessage(message: error.localizedDescription, inViewConroller: self, animated: true, hideAfter: 3)
                                    
                                }
                                break
                            }
                        })
                }
            }
                
            else
            {
                let refreshAlert = UIAlertController(title: "Error", message: "Internet not Available", preferredStyle: UIAlertControllerStyle.alert)
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    print("Handle Ok logic here")
                }))
                refreshAlert.view.tintColor = UIColor.red
                
                present(refreshAlert, animated: true, completion: nil)
                //showHudWithMessage(message: "Internet not Available!", inViewConroller: self, animated: true, hideAfter: 2)
            }
        }
    }
    
    //Mark: Functions
    func checkForValidation() -> Bool {
        
        // Get text from textfield
        let user_id        = self.TxtuserId.text?.trim()
        let pass           = self.TxtPassword.text?.trim()
        if  user_id == "" && pass == ""
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Enter UserId and Password!", preferredStyle: UIAlertControllerStyle.alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
            }))
            refreshAlert.view.tintColor = UIColor.red
            
            present(refreshAlert, animated: true, completion: nil)
            return false
        }
      else if  user_id == ""
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Enter UserId!", preferredStyle: UIAlertControllerStyle.alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
            }))
            refreshAlert.view.tintColor = UIColor.red
            
            present(refreshAlert, animated: true, completion: nil)
            return false
        }
        else if  pass == ""
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Enter Password!", preferredStyle: UIAlertControllerStyle.alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
            }))
            refreshAlert.view.tintColor = UIColor.red
            
            present(refreshAlert, animated: true, completion: nil)
            return false
        }
        
        return true
    }

}
