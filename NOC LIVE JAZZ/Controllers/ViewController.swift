//
//  ViewController.swift
//  NOC LIVE JAZZ
//
//  Created by macbook on 10/10/2017.
//  Copyright © 2017 macbook. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction func buttonACtion(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "vcViewController") as! vcViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
   
    
}

