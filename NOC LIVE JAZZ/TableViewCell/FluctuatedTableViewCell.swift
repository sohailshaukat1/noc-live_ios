//
//  FluctuatedTableViewCell.swift
//  NOC LIVE JAZZ
//
//  Created by Mac on 10/17/17.
//  Copyright © 2017 macbook. All rights reserved.
//

import UIKit

class FluctuatedTableViewCell: UITableViewCell {
    @IBOutlet weak var lbl_mainSitecode: UILabel!
    
    @IBOutlet weak var lbl_sitecode: UILabel!
    @IBOutlet weak var lbl_complete: UILabel!
    @IBOutlet weak var lbl_total: UILabel!
    @IBOutlet weak var lbl_partial: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
